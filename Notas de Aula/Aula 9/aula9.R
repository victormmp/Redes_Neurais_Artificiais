rm(list=ls())
library("RSNNS")

N <- 100
x <- runif(N,0,2*pi)
y <- sin(x) + rnorm(N,0,0.1)
yt <- sin(x)


rede <- mlp(x, y, size = 3, maxit = 200, initFunc = "Randomize_Weights",
            initFuncParams = c(-0.3, 0.3), learnFunc = "Rprop",
            learnFuncParams = c(0.1, 0.1), updateFunc = "Topological_Order",
            updateFuncParams = c(0), hiddenActFunc = "Act_Logistic",
            shufflePatterns = TRUE, linOut = TRUE)

xt <- seq(0,2*pi,0.01)
yseno <- sin(xt)
yhat <- predict(rede,as.matrix(xt))

par(mfrow=c(1,2))
plot(x,y,xlim = c(0,2*pi),ylim=c(-1.5,1.5),xlab='',ylab='')
par(new=T)
plot(xt,yhat,type='l',col='blue',xlim = c(0,2*pi),ylim=c(-1.5,1.5),xlab='',ylab='')
par(new=T)
plot(xt,yseno,type='l',col='red',xlim = c(0,2*pi),ylim=c(-1.5,1.5),xlab='x',ylab='sin(x)')

plot(rede$IterativeFitError,type='l',main='(b)',xlab = 'Época',ylab='Erro de Treinamento')

# USO DO PACKAGE EM CLASSIFICAÇÕES

data(iris)
#source(iris)

ntrain <- 35
xc1 <- iris[1:50,1:4]
xc2 <- iris[51:100,1:4]
xc3 <- iris[101:150,1:4]

rede <- mlp(x, y, size = 3, maxit = 200, initFunc = "Randomize_Weights",
            initFuncParams = c(-0.3, 0.3), learnFunc = "Rprop",
            learnFuncParams = c(0.1, 0.1), updateFunc = "Topological_Order",
            updateFuncParams = c(0), hiddenActFunc = "Act_Logistic",
            shufflePatterns = TRUE, linOut = TRUE)

