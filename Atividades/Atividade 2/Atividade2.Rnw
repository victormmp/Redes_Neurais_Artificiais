\documentclass[12pt,a4paper,titlepage]{article}
\usepackage[utf8x]{inputenc}
\usepackage{ucs}
\usepackage[portuguese]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{geometry}
\usepackage{float}
\usepackage{makecell}
\usepackage{multirow}
%\usepackage{hyperref}
\usepackage[table,xcdraw]{xcolor}
%\usepackage{caption}
%\usepackage{subcaption}
%\usepackege[nogin]{sweave}

\graphicspath{{Imagens/}}


% Mudando a fonte do documento para Times New Roman (ptm)
\renewcommand*\rmdefault{ptm}

% Novo comando para inserir fraÃÂ§ÃÂ£o inclinada
\newcommand*\rfrac[2]{{}^{#1}\!/_{#2}}

\pagestyle{fancy}
\fancyhf{}
\lhead[]{Redes Neurais Artificiais}
\rhead[]{Atividade 2}
\chead[]{}

\lfoot{Página \thepage \enspace de \pageref{LastPage} }
\rfoot{\leftmark}
\renewcommand{\footrulewidth}{1pt}


\begin{document}
\SweaveOpts{concordance=TRUE}
	\begin{titlepage}
		
		
		\newgeometry{top=2cm, bottom=2cm, left=2cm, right=2cm}
		
\centering
{\scshape\Large Universidade Federal de Minas Gerais \par}
{\scshape\large Redes Neurais Artificiais \par}
\vspace{4cm}
{\huge \textbf{Atividade 2}\par}
{\Large Perceptron Multicamadas (MLP) \par}
\vspace{3cm}
{\Large\itshape Victor Marcius Magalhãees Pinto\\Mat: 2013031240\par}
\vfill


% Bottom of the page
{\large Abril 2017\par}
\end{titlepage}
\restoregeometry

%*******************SUMÃRIO*********************
\renewcommand*\contentsname{Sumário}
\tableofcontents
\newpage
%***********************************************

% ===============================================================================
% CONTEÚDO

\section{Descrição do Problema}

Para esta atividade, o objetivo é criar um separador de forma a classificar o resultado de uma operação de XOR (ou exclusivo). Analiticamente, o resultado de uma operação dessas tem a forma:

\begin{table}[H]
\centering
\caption{Tabela-Verdade de uma operação XOR}
\label{tab: xor}
\begin{tabular}{ccc}
\hline
\multicolumn{2}{c}{\textbf{Entradas}} & \multirow{2}{*}{\textbf{Saída}} \\ \cline{1-2}
\textbf{$x_1$} & \textbf{$x_2$} &  \\ \hline
0 & \multicolumn{1}{c|}{0} & 0 \\
0 & \multicolumn{1}{c|}{1} & 1 \\
1 & \multicolumn{1}{c|}{0} & 1 \\
1 & \multicolumn{1}{c|}{1} & 0 \\ \hline
\end{tabular}
\end{table}

o que nos fornece, graficamente, o sistema a seguir:
\begin{center}
<<echo=F,fig=T>>=
rm(list=ls())
x<-matrix(c(0,0,0,1,1,0,1,1),ncol = 2,byrow = T)
xaug<-cbind(x,1)
y<-matrix(c(0,1,1,0),ncol = 1)

plot(x[1,1],x[1,2],col='red',type='p',xlim=c(0,1),ylim=c(0,1),xlab='',ylab='')
par(new=T)
plot(x[2,1],x[2,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(x[3,1],x[3,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(x[4,1],x[4,2],col='red',type='p',xlim=c(0,1),ylim = c(0,1),xlab='x1',ylab='x2')
grid()
@
\end{center}

Para os sistema acima, é impossível separar as amostras diretamente utilizando uma equação, dada as posições das distribuições das amostras. Portanto, para separarmos as mesmas, antes torna-se necessário fazer seu mapeamento para outro sistema de coordenadas, utilizando de uma função de camada intermediária que é responsável por linearizar o problema em questão, para a camada de saída.

Consideremos agora a análise em partes da função XOR, como no circuito abaixo:

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{circuito_Logico.png}
\caption{Circuito lógico.}
\end{center}
\end{figure}

Sendo $h_1$ e $h_2$ as saídas intermédiárias do circuito, fruto respectivamente das operações:

\begin{subequations}
	\begin{align}
h_1 &= \overline{x_1}x_2\\
h_2 &= x_1\overline{x_2}
\end{align}
\end{subequations}

o que nos leva ao sistema, em função de $h_1$ e $h_2$:

\begin{table}[H]
\centering
\caption{Tabela Verdade da operação XOR considerando operações intermediárias}
\label{tab: xor intermed}
\begin{tabular}{cccll}
\hline
\multicolumn{2}{c}{\textbf{Entradas}} & \multirow{2}{*}{\textbf{Saída}} & \multicolumn{1}{c}{\multirow{2}{*}{\textbf{$h_1$}}} & \multicolumn{1}{c}{\multirow{2}{*}{\textbf{$h_2$}}} \\ \cline{1-2}
\textbf{$x_1$} & \textbf{$x_2$} &  & \multicolumn{1}{c}{} & \multicolumn{1}{c}{} \\ \hline
0 & \multicolumn{1}{c|}{0} & \multicolumn{1}{c|}{0} & 0 & 0 \\
0 & \multicolumn{1}{c|}{1} & \multicolumn{1}{c|}{1} & 1 & 0 \\
1 & \multicolumn{1}{c|}{0} & \multicolumn{1}{c|}{1} & 0 & 1 \\
1 & \multicolumn{1}{c|}{1} & \multicolumn{1}{c|}{0} & 0 & 0 \\ \hline
\end{tabular}
\end{table}

\newpage
\section{Código}

Para começar, geramos as amostras com base nos resultados esperados da função. Para resultados de valor 1, utilizamos a cor azul, e vermelho para as saídas em 0.

<<echo=T,fig=T>>=
rm(list=ls())
library("plot3D")


x<-matrix(c(0,0,0,1,1,0,1,1),ncol = 2,byrow = T)
xaug<-cbind(x,1)
y<-matrix(c(0,1,1,0),ncol = 1)

plot(x[1,1],x[1,2],col='red',type='p',xlim=c(0,1),ylim=c(0,1),xlab='',ylab='')
par(new=T)
plot(x[2,1],x[2,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(x[3,1],x[3,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(x[4,1],x[4,2],col='red',type='p',xlim=c(0,1),ylim = c(0,1),xlab='x1',ylab='x2')
grid()

@

Para realizar o mapeamento, criamos dois neurônios perceptron relativos aos separadores lineares de equação: 
\begin{equation}
x_2 = x_1 +0.5
\end{equation}

\noindent
com $w_1 = -1$, $w_2 = 1$ e $w_0 = -0.5$; e:
\begin{equation}
x_2 = x_1 -0.5
\end{equation}

\noindent
com $w_1 = -1$, $w_2 = 1$ e $w_0 = 0.5$. Os separadores são definidos da forma:

<<echoi=T,fig=T>>=

#vetores de pesos sinápticos w, para as retas de separação
w1<-matrix(c(-1,1,-0.5),ncol = 1)
w2<-matrix(c(-1,1,0.5),ncol = 1)

xt<-seq(0,1,0.1)
y1<-xt-0.5
y2<-xt+0.5

plot(x[1,1],x[1,2],col='red',type='p',xlim=c(0,1),ylim=c(0,1),xlab='',ylab='')
par(new=T)
plot(x[2,1],x[2,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(x[3,1],x[3,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(x[4,1],x[4,2],col='red',type='p',xlim=c(0,1),ylim = c(0,1),xlab='x1',ylab='x2')
grid()

par(new=T)
plot(xt,y1,col='red',type='l',xlim=c(0,1),ylim = c(0,1))
par(new=T)
plot(xt,y2,col='red',type='l',xlim=c(0,1),ylim = c(0,1))
@

Considerando que a reta superior mapeia os valores para o parâmetro $h_1$ e que a reta de baixo mapeia os parâmetros para $h_2$, se considerarmos que para cada amostra, os seus valores de $h_1$ serão 0 caso as amostras se encontrem abaixo da sua reta correspondente, e 1 caso estejam acima; os seus valores de $h_2$ serão 1 caso as amostras se encontrem abaixo da sua reta correspondente, e 0 caso estejam acima. Podemos transferir as mesmas para o novo sistema de coordenadas, como descrito pela tabela \ref{tab: xor intermed}, da forma:

<<echo=T,fig=T>>=

h1<- 1*((xaug %*% w1) >=0)
h2<- 1*((xaug %*% w2) <=0)

H<-cbind(h1,h2)

plot(H[1,1],H[1,2],col='red',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(H[2,1],H[2,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(H[3,1],H[3,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(H[4,1],H[4,2],col='red',type='p',xlim=c(0,1),ylim = c(0,1),xlab='h1',ylab='h2')
grid()
@

Agora podemos construir um separador linear para classificar as amostras neste novo sistema. Para tal, podemos definir um neurônio simples com separador linear:

\begin{equation}
x2 = - x1 + 0.5
\end{equation}

\noindent
com os parâmetros $w_1 = 1$, $w_2 = 1$ e $w_0 = -0.5$. 

<<echo=T,fig=T>>=

w3<-matrix(c(1,1,-0.5),ncol = 1)
y3 <- -xt + 0.5

plot(H[1,1],H[1,2],col='red',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(H[2,1],H[2,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(H[3,1],H[3,2],col='blue',type='p',xlim=c(0,1),ylim = c(0,1),xlab='',ylab='')
par(new=T)
plot(H[4,1],H[4,2],col='red',type='p',xlim=c(0,1),ylim = c(0,1),xlab='h1',ylab='h2')
grid()

par(new=T)
plot(xt,y3,col='red',type='l',xlim=c(0,1),ylim =c(0,1),xlab='',ylab='')

@

A superfície de separação para as amostras linearizadas, para a camada de saída, será:

<<echo=T,fig=T>>=

Haug<-cbind(H,1)
h3<- 1*((Haug %*% w3) >=0)

seqi <- seq(0,1,0.1)
seqj <- seq(0,1,0.1)

M1 <- matrix(1,nrow = length(seqi),ncol = length(seqj))

ci <- 0
for (i in seqi){
  ci <- ci+1
  cj <- 0
  for (j in seqj){
    cj <- cj+1
    xaux <- c(i,j,1)
    M1[ci,cj] <- 1*((t(w3) %*% xaux)>=0)
  }
}

persp3D(seqi,seqj,M1,contour=T)
@

E para o perceptron de múltilpas camadas, desde os neurônios de entrada até os de saída, será:

<<echo=T,fig=T>>=

#library('rgl')

M2 <- matrix(1,nrow = length(seqi),ncol = length(seqj))

ci <- 0
for (i in seqi){
  ci <- ci+1
  cj <- 0
  for (j in seqj){
    cj <- cj+1
    xaux <- c(i,j,1)
    M2[ci,cj] <- 1*(((t(w1) %*% xaux)>=0) | ((t(w2) %*% xaux)<=0))
  }
}

persp3D(seqi,seqj,M2,contour=T)
@

\end{document}